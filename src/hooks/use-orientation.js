import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';

const useOrientation = () => {
    const [screenInfo, setScreenInfo] = useState(Dimensions.get('screen'));

    useEffect(() => {
        const onChange = (result) => {
            setScreenInfo(result.screen);
        }

        const changeEvent = Dimensions.addEventListener('change', onChange);

        return () => changeEvent.remove();
    }, []);

    return {
        ...screenInfo,
        isPortrait: screenInfo.height > screenInfo.width
    };
}
export default useOrientation;
