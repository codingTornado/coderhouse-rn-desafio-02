import React, { useState } from "react";
import { SafeAreaView } from "react-native";

import StartGameScreen from "./screens/start-game-screen";
import GameScreen from "./screens/game-screen";
import GameOverScreen from "./screens/game-over-screen";

import { styles } from "./styles";

const App = () => {
  const [gameStarted, setGameStarted] = useState(false);
  const [guessTaken, setGuessTaken] = useState();

  const gameStart = () => {
    setGameStarted(true);
  };

  const restartHandler = () => {
    setGuessTaken(undefined);
  };

  let content = <StartGameScreen startHandler={gameStart} />

  if (gameStarted) {
    content = <GameScreen choiceCallback={setGuessTaken} />
  }

  if (guessTaken !== undefined) {
    content = <GameOverScreen
      onRestart={restartHandler}
      wasSuccessful={guessTaken}
      />
  }

  return (
    <SafeAreaView style={styles.container}>
      {content}
    </SafeAreaView>
  );
};


export default App;
