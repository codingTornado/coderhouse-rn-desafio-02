import { StyleSheet } from "react-native"
import { colors, margin, padding } from "../../constants/theme";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  card: {
    backgroundColor: colors.background,
    borderBottomColor: colors.highlights,
    borderBottomWidth: 1,
    color: colors.foreground,
    elevation: 2,
    justifyContent: "center",
    margin: margin.small,
    padding: padding.medium,
  },
});
