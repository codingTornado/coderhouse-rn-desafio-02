import React from "react";
import { Button, View, Text } from "react-native";
import { styles } from "./styles";

const GameInterface = ({ number, buttonHandlers }) => {

  const buttonDisplay = () => {
    if (!number) {
      return (
        <View style={styles.buttonRow}>
          <Button
            title="Comenzar"
            color={styles.button.color}
            onPress={buttonHandlers.roll}
          />
        </View>
      );
    }

    return (
      <View style={styles.buttonRow}>
        <Button
          title="Mayor"
          color={styles.button.color}
          onPress={buttonHandlers.ge}
          />

        <Button
          title="Menor"
          color={styles.button.color}
          onPress={buttonHandlers.le}
          />
      </View>
    );
  };

  return (
    <View style={styles.centered}>
      <Text style={styles.title}>{number ? number : "Roll!!!"}</Text>
      {buttonDisplay()}

    </View>
  )
}

export default GameInterface;
