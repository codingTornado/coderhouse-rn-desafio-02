import { StyleSheet } from "react-native";

import { colors, margin } from "../../constants/theme";

export const styles = StyleSheet.create({
  button: {
    color: colors.primary,
  },

  buttonRow: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
  },

  centered: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-evenly",
    flexGrow: 1,
    width: "100%",
  },

  title: {
    fontFamily: "Montserrat-Bold",
    fontSize: 24,
    textAlign: "center",
    color: colors.primaryTitle,
    marginBottom: margin.medium,
  }
});
