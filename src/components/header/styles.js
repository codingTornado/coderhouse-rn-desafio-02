import { StyleSheet } from 'react-native'

import { colors, padding } from "../../constants/theme";

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.softBackground,
    padding: padding.small,
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
    textAlign: 'center',
    margin: 10,
    color: colors.primaryTitle,
  }
});
