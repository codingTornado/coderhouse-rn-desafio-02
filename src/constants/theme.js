export const colors = {
  primaryTitle: '#B8BB26',
  secondaryTitle: '#FABD2F',

  foreground: '#EBDBB2',
  foregroundAlt: '#FBF1C7',

  hardBackground: '#1D2021',
  background: '#282828',
  softBackground: '#32302F',

  highlights: "#A89984",
  primary: "#076678",
}

export const fontSize = {
  title: 20,
  text: 16,
  button: 18,
  large: 35
}

export const padding = {
  small: 10,
  medium: 20,
  large: 30
}

export const margin = {
  small: 10,
  medium: 20,
  large: 30
}
