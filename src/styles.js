import { StyleSheet } from 'react-native'
import { colors } from "./constants/theme";

export const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.hardBackground,
    flex: 1,
  }
});
