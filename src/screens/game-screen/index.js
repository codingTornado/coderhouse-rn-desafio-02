import React, { useState } from 'react';
import {
  ScrollView,
  View,
} from 'react-native';

import { styles } from './style';

import Card from '../../components/card';
import GameInterace from "../../components/game-interface";
import Header from '../../components/header';

const rollDice = () => {
  return Math.floor(Math.random() * (10 - 1) + 1);
}

const GameScreen = ({ choiceCallback }) => {
  const [userNumber, setUserNumber] = useState();
  const [otherNumber, setOtherNumber] = useState();

  const rollHandler = () => {
    setUserNumber(rollDice());
    setOtherNumber(rollDice());
  }

  const geHandler = () => {
    choiceCallback({
      guess: userNumber >= otherNumber,
      text: `${userNumber} ≥ ${otherNumber}`,
    });
  }

  const leHandler = () => {
    choiceCallback({
      guess: userNumber <= otherNumber,
      text: `${userNumber} ≤ ${otherNumber}`,
    });
  }

  return (
    <ScrollView style={styles.container}>
      <View style={styles.container}>
        <Header title="¡¡Los daditos!!" />
        <Card>
          <GameInterace
            number={userNumber}
            buttonHandlers={{
              roll: rollHandler,
              ge: geHandler,
              le: leHandler,
            }} />
        </Card>
      </View>
    </ScrollView>
  )
}

export default GameScreen;
