import { StyleSheet } from "react-native";

import { colors, fontSize, margin } from "../../constants/theme";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },

  title: {
    color: colors.primaryTitle,
    fontSize: fontSize.title,
    marginBottom: margin.medium,
  }
})
