import React from "react"
import { View, Text, Button } from "react-native"
import Card from "../../components/card";
import { styles } from "./styles";

const GameOverScreen = ({ onRestart, wasSuccessful }) => {
  let choice = wasSuccessful.guess ? "Ganaste" : "Perdiste";

  return (
    <View style={styles.container}>
      <Card style={styles.cardContainer}>
        <Text style={styles.choice}>{wasSuccessful.text}</Text>
        <Text style={styles.choice}>¡{choice}!</Text>
      </Card>
      <Button
        color={styles.button.color}
        title="Reiniciar"
        onPress={onRestart}
        />
    </View>
  )
}

export default GameOverScreen;
