import { StyleSheet, Dimensions } from "react-native";
import { margin, fontSize, colors } from "../../constants/theme";
const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  cardContainer: {
    width: width / 2,
    height: height * 0.25,
    marginVertical: margin.large,
    justifyContent: "center",
    alignItems: "center",
  },

  choice: {
    fontSize: fontSize.large,
    color: colors.primaryTitle,
  },

  button: {
    color: colors.primary,
  },
});
