import { StyleSheet } from "react-native"
import { colors, fontSize, margin } from "../../constants/theme";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  cardText: {
    fontSize: fontSize.text,
    textAlign: "center",
    marginVertical: margin.medium,
    color: colors.foreground,
  },

  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginHorizontal: margin.medium,
  },

  button: {
    color: colors.primary,
  },
});
