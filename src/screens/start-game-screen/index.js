import React from "react";
import {
  Button,
  ScrollView,
  Text,
  View,
} from "react-native";

import Card from "../../components/card";
import Header from "../../components/header";

import { styles } from "./styles";

const StartGameScreen = ({ startHandler }) => {
  return (
    <ScrollView style={styles.container}>
      <View style={styles.container}>
        <Header title="Rollies!!" />
        <Card>
          <Text style={styles.cardText}>
            Rollies es un juego de azar y estimación de probabilidades.
          </Text>
          <Text style={styles.cardText}>
            El jugador tira un dado y apuesta a si el número obtenido es
            mayor o menor al de la máquina. Para esta implementación vamos a
            jugar con d10, es decir que los valores posibles están en el
            rango [1, 10] ∈ ℕ
          </Text>
          <View style={styles.buttonsContainer}>
            <Button
              title="Comenzar"
              color={styles.button.color}
              onPress={startHandler}
              />
          </View>
        </Card>
      </View>
    </ScrollView>
  )
}

export default StartGameScreen;
